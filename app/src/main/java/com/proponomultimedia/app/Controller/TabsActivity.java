package com.proponomultimedia.app.Controller;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.proponomultimedia.R;
import com.proponomultimedia.app.Model.Entity;
import com.proponomultimedia.app.Model.Foto_Entity;
import com.proponomultimedia.app.Model.News;
import com.proponomultimedia.app.Utilities.AudioAdapter;
import com.proponomultimedia.app.Utilities.CameraAdapter;
import com.proponomultimedia.app.Utilities.CardAdapter;
import com.proponomultimedia.app.Utilities.FotoAdapter;
import com.proponomultimedia.app.Utilities.NetworkConnection;
import com.proponomultimedia.app.Utilities.NewsAdapter;
import com.proponomultimedia.app.Utilities.ShoutCast;
import com.proponomultimedia.app.Utilities.VideoAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class TabsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final String LOG = "MyActivity";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private View decorView;
    private int uiOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        setContentView(R.layout.tabs_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final CollapsingToolbarLayout collapse_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        collapse_toolbar.setTitleEnabled(false);
        collapse_toolbar.setBackgroundResource(R.mipmap.about_us_image);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(mSectionsPagerAdapter.getTabView(i));
        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mViewPager.setCurrentItem(tab.getPosition());

                switch (tab.getPosition()) {
                    case 0:
                        collapse_toolbar.setBackgroundResource(R.mipmap.about_us_image);
                        break;
                    case 1:
                        collapse_toolbar.setBackgroundResource(R.mipmap.service_image);
                        break;
                    case 2:
                        collapse_toolbar.setBackgroundResource(R.mipmap.marketing_image);
                        break;
                    case 3:
                        collapse_toolbar.setBackgroundResource(R.mipmap.news_image);
                        break;
                    case 4:
                        collapse_toolbar.setBackgroundResource(R.mipmap.contact_image);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Service Fragment.
     */
    public static class ServiceFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView foto_list, panoram_list, camera_list, video_list, audio_features_list, audio_list;
        private RecyclerView.LayoutManager foto_LayoutManager, panoram_LayoutManager, camera_LayoutManager, video_LayoutManager, audio_features_LayoutManager, audio_LayoutManager ;

        public ServiceFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ServiceFragment newInstance(int sectionNumber) {
            ServiceFragment fragment = new ServiceFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_service, container, false);
            camera_list = (RecyclerView) rootView.findViewById(R.id.camera_list);
            video_list = (RecyclerView) rootView.findViewById(R.id.video_list);


            camera_LayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            camera_list.setLayoutManager(camera_LayoutManager);
            camera_list.setItemAnimator(new DefaultItemAnimator());

            video_LayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            video_list.setLayoutManager(video_LayoutManager);
            video_list.setItemAnimator(new DefaultItemAnimator());


            showSectionBody();

            return rootView;
        }

        public void showSectionBody(){


            new AsyncTask() {


                private CameraAdapter camera_adapter = null;
                private VideoAdapter video_adapter = null;


                @Override
                protected String doInBackground(Object[] params) {


                    /////////////////////////////////Video section/////////////////////////////////
                    ArrayList<Foto_Entity> camera_al = new ArrayList<Foto_Entity>();
                    camera_al.add(new Foto_Entity(R.mipmap.sony_a7s_ll_propono, "KAMERE SONY A7S II", "Inovativna Sony a7s II kamera, 35 mm CMOS senzorom uparena sa BIONT X procesorom nevjerovatnim dinamičkim rasponom i niskom raziniom šuma.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.propono_inspire, "INOVATIVNI DJI INSPIRE 1", "Sofisticirani quadcopter DJI Inspire 1 omogućit će vam profesionalne snimke i fotografije visoke kvalitete kao da ste ih snimali iz helikoptera.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.ronin_m_propono, "DJI RONIN STEADYCAM", "Elektronska stabilizacija slike u 3 osi, kompaktan, lagan i vrlo jednostavan za korištenje. Ronin-M daje vam ono što svaki filmaš sanja: sloboda.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.zoom_h6_pro_recorder, "ZOOM H6 AUDIO REKORDER", "Zoom-ov profesionalni snimač zvuka H6 Vam daje sve što Vam je potrebno za snimanje kvalitetnog audio zvuka.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.trivago, "SET TRAVIGO SLIDER + HDN PRO DC MOTOR", "Slidekamera HDN DC / DC HDN PRO pogona, namijenjen za Slidekamera uređajima, oprema posebno dizajnirana za glatke metak u pokretu u rasponu od 2 mm / s do 55mm / s, kao i Ubrzavanje fotografija.Tih rad smanjuje buku na minimum na snimljeni materijal.Osim toga, u uređaj je vrlo lagana, njegova težina ne prelazi 1 kg.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.big_x_curve_2_, "SMART EYE X-CURVE SP", "Priručnik X-krivulja sustav omogućuje automatsko kontinuirano praćenje objekta od strane kamere / photocamera tijekom kretanja cart.Thanks u sustavu X-krivulje objekta koji se snima i dalje cijelo vrijeme u središtu okvira.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.sony_a7r_mark_ii_digital, "KAMERE SONY A7R II", "Uz prvi svjetski full-frame 42,4 megapiksela Exmor R back-osvjetljene zgrade CMOS senzorom, Alfa a7R II Mirrorless digitalni fotoaparat Sony je spreman uzeti mirrorless snimanje na drugu razinu.", ""));
                    camera_al.add(new Foto_Entity(R.mipmap.shogun_inferno, "PROFESIONALNO 4K ORUZJE ZA PROFESIONALNE 4K KAMERE", "Profesionalno snimanje, reprodukcija, praćenje i uređivanje koji će osloboditi skriveni potencijal fotoaparata DSLR u Cinema", ""));

                    camera_adapter = new CameraAdapter(getActivity(),camera_al);

                    ArrayList<Foto_Entity> video_al = new ArrayList<Foto_Entity>();
                    video_al.add(new Foto_Entity(R.mipmap.club_petar_pan, "Club Petar Pan : Makarska ::", "promo video 2016", "o_J4MhOfCV4"));
                    video_al.add(new Foto_Entity(R.mipmap.official_movie_highlights_wta_bol_open_2016, "Official Movie Highlights", "WTA BOL OPEN 2016", "jxcXIj-9hq4"));
                    video_al.add(new Foto_Entity(R.mipmap.hotel_split_official_video, "Hotel Split", "Official video", "hHfH7HOF1rw"));
                    video_al.add(new Foto_Entity(R.mipmap.jole_pijanica_nisam, "Jole - Pijanica nisam", "(Official video)", "O4P4O8wdUJs"));
                    video_al.add(new Foto_Entity(R.mipmap.matko_i_brane_trosi_i_uzivaj, "Matko i Brane - Troši i uživaj", "(Official)", "DeWUPOwbB7s"));
                    video_al.add(new Foto_Entity(R.mipmap.joes_bar_kasjuni, "Joes bar Kašjuni Split..’till the sunset", "(Official 4K)", "oX3Zsxil1Gw"));
                    video_al.add(new Foto_Entity(R.mipmap.villa_jankovic_bacvice, "Villa Janković", "Bačvice", "OPIKQqdkdmo"));
                    video_al.add(new Foto_Entity(R.mipmap.jahta_pida, "Jahta", "PIDA", "rgtKIoPUuC0"));
                    video_al.add(new Foto_Entity(R.mipmap.klapa_kase, "Klapa Kaše - Tamo di brnistra cvate", "(Official 4K)", "BpiLh7Skr4k"));
                    video_al.add(new Foto_Entity(R.mipmap.cox_bar, "COX bar", "opening summer party", "CXnHUPjcYwM"));
                    video_adapter = new VideoAdapter(getActivity(),video_al);

                    return "";
                }

                @Override
                protected void onPostExecute(Object s) {
                    super.onPostExecute(s);



                    if(camera_adapter != null){

                        camera_list.setAdapter(camera_adapter);


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "camera adapter null",  Toast.LENGTH_LONG).show();


                    }

                    if(video_adapter != null){

                        video_list.setAdapter(video_adapter);


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "video adapter null",  Toast.LENGTH_LONG).show();


                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);



        }

    }

    /**
     * About Us Fragment.
     */
    public static class AboutUsFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private TextView section_head, section_body1, section_body2, section_body3;
        private RecyclerView cv_list;
        private RecyclerView.LayoutManager mLayoutManager;


        public AboutUsFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static AboutUsFragment newInstance(int sectionNumber) {
            AboutUsFragment fragment = new AboutUsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
            section_body1 = (TextView) rootView.findViewById(R.id.section_body1);
            section_body2 = (TextView) rootView.findViewById(R.id.section_body2);
            section_body3 = (TextView) rootView.findViewById(R.id.section_body3);
            section_head = (TextView) rootView.findViewById(R.id.section_head);
            cv_list = (RecyclerView) rootView.findViewById(R.id.cv_list);
            mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            cv_list.setLayoutManager(mLayoutManager);
            cv_list.setItemAnimator(new DefaultItemAnimator());

            section_head.setText("Vizualne komunikacije, produkcija, multimedia!");

            section_body1.setText("PROPONO d.o.o. multimedia Split");
            section_body2.setText("Obratite nam se s Vašom potrebom!");
            section_body3.setText(Html.fromHtml("Općenito se bavimo <b>multimediom</b>, prezentacijama, izrađujemo internet stranice, digitalna foto video produkcija, te grafički dizajn i priprema za print. Posjedujemo profesionalnu opremu za snimanje i fotografiranje, a najnovije u ponudi je snimanje i fotografiranje iz zraka. <b>Multimedia</b> je zajednički naziv za medije koji kombiniraju više pojedinačnih medija, da bi se stvorila jedna cjelina. U običnom govoru multimedia najčešće znači interaktivni kompjuterski projekt u kojem se koristi video, tekst i zvuk, kao što su npr. interaktivne prezentacije."));

            showSectionBody();

            return rootView;
        }


        public void showSectionBody(){


            new AsyncTask<RecyclerView, Void, CardAdapter>() {

                private RecyclerView v;


                @Override
                protected CardAdapter doInBackground(RecyclerView...params) {

                    v = params[0];
                    ArrayList<Entity> entities_al = new ArrayList<Entity>();


                    entities_al.add(new Entity("Profesionalnost", "Svakom zadatku pristupamo profesionalno te odgovorno ispunjavamo sve kriterije i prethodne dogovore"));
                    entities_al.add(new Entity("Brzina", "U svijetu multimedije brzina ima ključnu ulogu u poslovanju te mi sve naše obveze ispunjavamo u najkraćem mogućem roku."));
                    entities_al.add(new Entity("Dostupnost", "Komunikacija je ključ ka većini uspijeha te smo uvijek dostupni za sve upite i zahtjeve svim našim trenutnim i budućim klijentima."));
                    entities_al.add(new Entity("Kreativnost", "Kreativan tim mladih i iskusnih djelatnika i suradnika osigurava jedinstvenost naših multimedijalnih uradaka."));

                    CardAdapter card_adapter = new CardAdapter(getActivity(), entities_al);

                    return card_adapter;
                }

                @Override
                protected void onPostExecute(CardAdapter s) {
                    super.onPostExecute(s);

                    if(s != null){

                        v.setAdapter(s);


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "Something went wrong",  Toast.LENGTH_LONG).show();


                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, cv_list);



        }


    }

    /**
     * Marketing Fragment.
     */
    public static class MarketingFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView audio_features_list, audio_list;
        private RecyclerView.LayoutManager audio_features_LayoutManager, audio_LayoutManager;
        private ImageButton play_button;
        private ImageButton pause_button;
        private TextView artist_name;
        private TextView song_name;
        private TextView logo_radio;
        private String meta_an = "";
        private String [] meta_an_array;
        private Intent streamService;
        private static final String LOG = "MyActivity";
        public static final int MY_PERMISSIONS_REQUEST_PHONE_CALL = 123;

        public MarketingFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static MarketingFragment newInstance(int sectionNumber) {
            MarketingFragment fragment = new MarketingFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //Log.v(LOG, "onCreate radio called");
            streamService = new Intent(getActivity().getApplicationContext(), ShoutCast.class);
            checkPermission();

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_marketing, container, false);
            audio_features_list = (RecyclerView) rootView.findViewById(R.id.audio_features_list);
            audio_list = (RecyclerView) rootView.findViewById(R.id.audio_list);
            play_button = (ImageButton) rootView.findViewById(R.id.play_button);
            pause_button = (ImageButton) rootView.findViewById(R.id.pause_button);
            artist_name = (TextView) rootView.findViewById(R.id.artist_name);
            song_name = (TextView) rootView.findViewById(R.id.song_name);
            logo_radio = (TextView) rootView.findViewById(R.id.logo_radio);

            audio_features_LayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            audio_features_list.setLayoutManager(audio_features_LayoutManager);
            audio_features_list.setItemAnimator(new DefaultItemAnimator());

            audio_LayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            audio_list.setLayoutManager(audio_LayoutManager);
            audio_list.setItemAnimator(new DefaultItemAnimator());
            Typeface MyCustomFont = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(),"fonts/HelveticaNeue-Bold.ttf");
            artist_name.setTypeface(MyCustomFont);
            logo_radio.setTypeface(MyCustomFont);

            play_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PlayStream();

                }
            });

            pause_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PauseStream();

                }
            });

            showSectionBody();

            return rootView;
        }

        @Override
        public void onStart() {
            super.onStart();
            //Log.v(LOG, "onStartFragment called");
            getActivity().getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter("main_activity"));

        }

        @Override
        public void onDestroy() {
            super.onDestroy();

            //Log.v(LOG, "onDestroyFragment called");
            //getActivity().getApplicationContext().stopService(streamService);
            getActivity().getApplicationContext().unregisterReceiver(mMessageReceiver);
        }

        void PlayStream(){

            getActivity().startService(streamService);
            play_button.setVisibility(View.INVISIBLE);
            pause_button.setEnabled(false);
            pause_button.setVisibility(View.VISIBLE);

        }

        void PauseStream(){

            getActivity().stopService(streamService);
            pause_button.setVisibility(View.INVISIBLE);
            play_button.setVisibility(View.VISIBLE);
            artist_name.setText("Listen");
            song_name.setText("live");

        }

        public void showSectionBody(){


            new AsyncTask() {

                private CameraAdapter audio_features_adapter = null;
                private AudioAdapter audio_adapter =  null;

                @Override
                protected String doInBackground(Object[] params) {


                    /////////////////////////////////Audio section/////////////////////////////////
                    ArrayList<Foto_Entity> audio_features_al = new ArrayList<Foto_Entity>();
                    audio_features_al.add(new Foto_Entity(R.mipmap.ic_check_black_24dp, "PRODUCIRAMO RADIO SPOTOVE, JINGLOVE, EMISIJE I REPORTAŽE", "Audio je važna podrška odjelu video produkcije, a u kreiranju proizvoda u najvećoj mjeri se sklada i koristi autorska glazba.", ""));
                    audio_features_al.add(new Foto_Entity(R.mipmap.ic_check_black_24dp, "USLUGE SINHRONIZACIJE, SNIMANJE VOKALA I PRODUKCIJE RADIJSKIH REKLAMA", "U procesu obrade zvučnih zapisa nudimo usluge sinhronizacije, snimanje vokala i produkcije radijskih reklama.", ""));
                    audio_features_al.add(new Foto_Entity(R.mipmap.ic_check_black_24dp, "RADIJSKI I TV VODITELJ", "Nudimo usluge vođenja raznih događaja, koncerta, skupova, event-a, TV emisija…radijski voditelj Edita Lučić Jelić.", ""));
                    audio_features_al.add(new Foto_Entity(R.mipmap.ic_check_black_24dp, "REFERENTNI RADOVI", "Gotove producirane spotove, kao agencija za promidžbu i marketing emitiramo na Radio Dalmaciji.", ""));
                    audio_features_adapter = new CameraAdapter(getActivity(), audio_features_al);

                    ArrayList<Foto_Entity> audio_al = new ArrayList<Foto_Entity>();
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Dalmacija cement prijateljima", "", "http://www.propono.hr/wp-content/uploads/2014/03/Dalmacija-cement-prijateljima.mp3"));
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Neda Ukraden DUGI novi", "", "http://www.propono.hr/wp-content/uploads/2014/03/Neda-Ukraden-DUGI-novi.mp3"));
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Spaladium Arena OTVARANJE - FJAKA BROS", "", "http://www.propono.hr/wp-content/uploads/2014/03/Spaladium-Arena-OTVARANJE.mp3"));
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Vela Luka CAMBI 2012", "", "http://www.propono.hr/wp-content/uploads/2014/03/Vela-Luka-CAMBI-2012.mp3"));
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Vela Luka MASLINA 2012", "", "http://www.propono.hr/wp-content/uploads/2014/03/Vela-Luka-MASLINA-2012.mp3"));
                    audio_al.add(new Foto_Entity(R.mipmap.audio, "Dalmacijanews 2013 koncert", "", "http://www.propono.hr/wp-content/uploads/2014/03/Dalmacijanews-2013-koncert.mp3"));
                    audio_adapter = new AudioAdapter(getActivity(), audio_al);

                    return "";
                }

                @Override
                protected void onPostExecute(Object s) {
                    super.onPostExecute(s);

                    if(audio_features_adapter != null){

                        audio_features_list.setAdapter(audio_features_adapter);


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "audio features adapter null",  Toast.LENGTH_LONG).show();


                    }

                    if(audio_adapter != null){

                        audio_list.setAdapter(audio_adapter);


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "audio adapter null",  Toast.LENGTH_LONG).show();


                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);



        }

        //This is the handler that will manager to process the broadcast intent
        private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                //Log.v(LOG, "onreceive called");
                //meta_an = prefs.getString("meta_title", "");

                int code = intent.getIntExtra("code", -1);

                if(code == 0){

                    meta_an = intent.getStringExtra("meta_title");

                    if(!meta_an.isEmpty() && meta_an.contains("-")){

                        meta_an_array = meta_an.split("-");
                        artist_name.setText(meta_an_array[0]);
                        song_name.setText(meta_an_array[1].trim());
                        pause_button.setEnabled(true);

                    }

                }else if(code == 1){

                    PauseStream();

                }



            }
        };

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public boolean checkPermission()
        {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if(currentAPIVersion >= android.os.Build.VERSION_CODES.M)
            {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_PHONE_STATE)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission necessary");
                        alertBuilder.setMessage("External storage permission is necessary");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_PHONE_CALL);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_PHONE_CALL);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }


        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_PHONE_CALL:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Toast.makeText(getActivity(),"Hvala ti", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(),"Morate dodijeliti dozvole za aplikaciju", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }



    }

    /**
     * News Fragment.
     */
    public static class NewsFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView news_list;
        private ProgressBar progressbar;
        private ProgressBar pb_load_more;
        private Button load_more_button;
        private RecyclerView.LayoutManager mLayoutManager;
        private ArrayList<News> news_array = new ArrayList<News>();
        private NetworkConnection nc;
        private String server_url = "http://www.propono.hr/api/?json=get_recent_posts&page=";
        private int page_number = 0;
        private NewsAdapter adapter = null;
        private String news_string = "";

        public NewsFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static NewsFragment newInstance(int sectionNumber) {
            NewsFragment fragment = new NewsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_news, container, false);
            news_list = (RecyclerView) rootView.findViewById(R.id.news_list);
            progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
            pb_load_more = (ProgressBar) rootView.findViewById(R.id.pb_load_more);
            load_more_button = (Button) rootView.findViewById(R.id.load_more_button);

            mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            news_list.setLayoutManager(mLayoutManager);
            news_list.setItemAnimator(new DefaultItemAnimator());
            //news_list.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

            adapter = new NewsAdapter(getActivity(), news_array);

            news_list.setAdapter(adapter);


            nc = new NetworkConnection();

            load_more_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    load_more_button.setVisibility(View.GONE);
                    pb_load_more.setVisibility(View.VISIBLE);
                    showNewsList();
                }
            });

            showNewsList();

            return rootView;
        }


        public void showNewsList(){


            new AsyncTask<RecyclerView, Void, NewsAdapter>() {

                private RecyclerView v;


                @Override
                protected NewsAdapter doInBackground(RecyclerView...params) {

                    v = params[0];
                    //NewsAdapter adapter = null;
                    page_number++;

                    news_string = nc.FetchData(server_url + page_number);
                    //Log.v(LOG, "page number: " + page_number);

                    if(news_string.isEmpty() || news_string.equals("No Internet Connection")){

                        adapter =  null;

                    }else{

                        try{

                            JSONObject news_jobject = new JSONObject(news_string);

                            JSONArray posts =  new JSONArray(news_jobject.getString("posts"));

                            for (int i = 0; i < posts.length(); i++) {

                                JSONObject post = (JSONObject) posts.get(i);

                                //news_array.add(new News(post.optJSONObject("thumbnail_images").optJSONObject("main-full").getString("url"), post.getString("title") , post.getString("content")));
                                adapter.insert(new News(post.optJSONObject("thumbnail_images").optJSONObject("full").getString("url"), post.getString("title") , post.getString("excerpt"), post.getString("content")));


                            }

                            //adapter = new NewsAdapter(getActivity(), news_array);


                        }catch (Exception e){e.printStackTrace();}


                    }


                    return adapter;
                }

                @Override
                protected void onPostExecute(NewsAdapter newsAdapter) {
                    super.onPostExecute(newsAdapter);

                    if(newsAdapter != null){

                        //v.setAdapter(newsAdapter);
                        adapter.notifyDataSetChanged();
                        progressbar.setVisibility(View.GONE);
                        pb_load_more.setVisibility(View.GONE);
                        load_more_button.setVisibility(View.VISIBLE);

                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "Something went wrong",  Toast.LENGTH_LONG).show();
                        progressbar.setVisibility(View.GONE);
                        page_number--;

                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, news_list);



        }

    }

    /**
     * Contact Fragment.
     */
    public static class ContactFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private CardView facebook, twitter, instagram, youtube, linkedin, google;

        public ContactFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ContactFragment newInstance(int sectionNumber) {
            ContactFragment fragment = new ContactFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
            facebook = (CardView)rootView.findViewById(R.id.facebook);
            twitter = (CardView)rootView.findViewById(R.id.twitter);
            youtube = (CardView)rootView.findViewById(R.id.youtube);
            instagram = (CardView)rootView.findViewById(R.id.instagram);
            linkedin = (CardView)rootView.findViewById(R.id.linkedin);
            google = (CardView)rootView.findViewById(R.id.google);

            facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://m.facebook.com/propono.multimedia");
                    getActivity().startActivity(intent);
                }
            });

            twitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://twitter.com/proponomultimed");
                    getActivity().startActivity(intent);
                }
            });

            instagram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://www.instagram.com/proponomultimedia/");
                    getActivity().startActivity(intent);
                }
            });

            youtube.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://www.youtube.com/user/PROPONOmultimedia");
                    getActivity().startActivity(intent);
                }
            });

            linkedin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://www.linkedin.com/in/propono-multimedia-a55a5490");
                    getActivity().startActivity(intent);
                }
            });

            google.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent =  new Intent(getActivity(), Browser.class);
                    intent.putExtra("url", "https://plus.google.com/112926376201682288478/about");
                    getActivity().startActivity(intent);
                }
            });


            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 0:
                    return AboutUsFragment.newInstance(position + 1);
                case 1:
                    return ServiceFragment.newInstance(position + 1);
                case 2:
                    return MarketingFragment.newInstance(position + 1);
                case 3:
                    return NewsFragment.newInstance(position + 1);
                case 4:
                    return ContactFragment.newInstance(position + 1);

            }

            return  null;

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        public View getTabView(int position) {
            switch (position) {
                case 0:
                    View AboutUsView = getLayoutInflater().inflate(R.layout.tab_design, null);
                    TextView about_us_title = (TextView) AboutUsView.findViewById(R.id.title);
                    ImageView about_us_image = (ImageView) AboutUsView.findViewById(R.id.icon);
                    about_us_title.setText("O NAMA");
                    about_us_image.setImageResource(R.mipmap.ic_home_white_24dp);
                    return AboutUsView;
                case 1:
                    View ServiceView = getLayoutInflater().inflate(R.layout.tab_design, null);
                    TextView service_title = (TextView) ServiceView.findViewById(R.id.title);
                    ImageView service_image = (ImageView) ServiceView.findViewById(R.id.icon);
                    service_title.setText("USLUGE");
                    service_image.setImageResource(R.mipmap.ic_business_center_white_24dp);
                    return ServiceView;
                case 2:
                    View MarketingView = getLayoutInflater().inflate(R.layout.tab_design, null);
                    TextView marketing_title = (TextView) MarketingView.findViewById(R.id.title);
                    ImageView marketing_image = (ImageView) MarketingView.findViewById(R.id.icon);
                    marketing_title.setText("MARKETING");
                    marketing_image.setImageResource(R.mipmap.ic_widgets_white_24dp);
                    return MarketingView;
                case 3:
                    View NewsView = getLayoutInflater().inflate(R.layout.tab_design, null);
                    TextView news_title = (TextView) NewsView.findViewById(R.id.title);
                    ImageView news_image = (ImageView) NewsView.findViewById(R.id.icon);
                    news_title.setText("NOVOSTI");
                    news_image.setImageResource(R.mipmap.ic_insert_drive_file_white_24dp);
                    return NewsView;
                case 4:
                    View ContactView = getLayoutInflater().inflate(R.layout.tab_design, null);
                    TextView contact_title = (TextView) ContactView.findViewById(R.id.title);
                    ImageView contact_image = (ImageView) ContactView.findViewById(R.id.icon);
                    contact_title.setText("KONTAKT");
                    contact_image.setImageResource(R.mipmap.ic_timeline_white_24dp);
                    return ContactView;
            }
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent streamService;
        streamService = new Intent(TabsActivity.this, ShoutCast.class);
        stopService(streamService);
    }
}
