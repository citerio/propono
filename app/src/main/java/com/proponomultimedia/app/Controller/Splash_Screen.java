package com.proponomultimedia.app.Controller;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

import com.proponomultimedia.R;

public class Splash_Screen extends AppCompatActivity {

    private VideoView video;
    private View decorView;
    private int uiOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        setContentView(R.layout.splash_screen);

        video = (VideoView)findViewById(R.id.video);

        Uri video_raw = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.propono_animation);

        if(video != null){

            video.setVideoURI(video_raw);
            video.setZOrderOnTop(true);

            video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    jump();
                }
            });

            video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    jump();
                    return false;
                }
            });

            video.start();

        }else {

            jump();

        }
    }

    public void jump(){

        Intent main_activity = new Intent(Splash_Screen.this, TabsActivity.class);
        startActivity(main_activity);
        finish();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(video.isPlaying()){

            video.stopPlayback();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);
        if(!video.isPlaying()){

            video.start();

        }
    }
}
