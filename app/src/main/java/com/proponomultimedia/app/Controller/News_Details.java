package com.proponomultimedia.app.Controller;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.proponomultimedia.R;
import com.squareup.picasso.Picasso;


public class News_Details extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "image";
    private static final String ARG_PARAM2 = "header";
    private static final String ARG_PARAM3 = "content";



    public News_Details() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static News_Details newInstance(String image, String header, String content) {
        News_Details fragment = new News_Details();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, image);
        args.putString(ARG_PARAM2, header);
        args.putString(ARG_PARAM3, content);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.news_details, container, false);

        ImageView image = (ImageView) rootView.findViewById(R.id.image);
        TextView header = (TextView) rootView.findViewById(R.id.header);
        WebView content = (WebView) rootView.findViewById(R.id.content);

        content.getSettings().setJavaScriptEnabled(true);
        content.setWebChromeClient(new WebChromeClient());
        //content.getSettings().setUseWideViewPort(true);
        content.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        content.getSettings().setLoadWithOverviewMode(true);
        content.getSettings().setUseWideViewPort(true);
        content.getSettings().setDefaultFontSize(34);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            content.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            content.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        content.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });

        Picasso.with(getActivity()).load(getArguments().getString(ARG_PARAM1)).error(R.mipmap.propono_logo).fit().centerInside().into(image);
        header.setText(Html.fromHtml(getArguments().getString(ARG_PARAM2)));
        //content.setText(Html.fromHtml(getArguments().getString(ARG_PARAM3)));
        content.loadData(getArguments().getString(ARG_PARAM3), "text/html; charset=UTF-8;", null);

        return rootView;
    }


}
