package com.proponomultimedia.app.Controller;

import android.media.AudioManager;
import android.media.MediaPlayer;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.TextView;

import com.proponomultimedia.R;

public class Audio_Player extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView title;
    private String file_title = "";
    private String file_url = "";
    private MediaPlayer mediaPlayer;
    private MediaController mediaController;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_player);

        file_title = getIntent().getStringExtra("file_title");
        file_url = getIntent().getStringExtra("file_url");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.title);

        title.setText(file_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mediaPlayer = new MediaPlayer();
        mediaController = new MediaController(Audio_Player.this);
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                mediaController.setMediaPlayer(new MediaController.MediaPlayerControl() {
                    @Override
                    public void start() {
                        mediaPlayer.start();
                    }

                    @Override
                    public void pause() {
                        mediaPlayer.pause();
                    }

                    @Override
                    public int getDuration() {
                        return mediaPlayer.getDuration();
                    }

                    @Override
                    public int getCurrentPosition() {
                        return mediaPlayer.getCurrentPosition();
                    }

                    @Override
                    public void seekTo(int pos) {
                        mediaPlayer.seekTo(pos);
                    }

                    @Override
                    public boolean isPlaying() {
                        return mediaPlayer.isPlaying();
                    }

                    @Override
                    public int getBufferPercentage() {
                        return 0;
                    }

                    @Override
                    public boolean canPause() {
                        return true;
                    }

                    @Override
                    public boolean canSeekBackward() {
                        return true;
                    }

                    @Override
                    public boolean canSeekForward() {
                        return true;
                    }

                    @Override
                    public int getAudioSessionId() {
                        return 0;
                    }
                });

                mediaController.setAnchorView(findViewById(R.id.audio_player_root));

                handler.post(new Runnable() {
                    public void run() {
                        mediaController.setEnabled(true);
                        mediaController.show();
                    }
                });

            }
        });

        try{

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(file_url);
            mediaPlayer.prepare(); // might take long! (for buffering, etc)
            mediaPlayer.start();

        }catch (Exception e){

            e.printStackTrace();

        }


    }


    @Override
    protected void onStop() {
        super.onStop();
        mediaController.hide();
        mediaPlayer.stop();
        mediaPlayer.release();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mediaController.show();
        return false;
    }
}
