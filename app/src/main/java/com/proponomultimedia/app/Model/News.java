package com.proponomultimedia.app.Model;

/**
 * Created by Jose Ricardo on 24/07/2016.
 */
public class News {

    private String image;
    private String header;
    private String excerpt;
    private String content;

    public News(String image, String header, String excerpt, String content) {
        this.image = image;
        this.header = header;
        this.excerpt = excerpt;
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }
}
