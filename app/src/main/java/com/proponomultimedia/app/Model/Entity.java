package com.proponomultimedia.app.Model;

/**
 * Created by Jose Ricardo on 27/02/2017.
 */
public class Entity {

    private String section_head;
    private String section_body;

    public Entity(String section_head, String section_body) {
        this.section_head = section_head;
        this.section_body = section_body;
    }

    public String getSection_head() {
        return section_head;
    }

    public void setSection_head(String section_head) {
        this.section_head = section_head;
    }

    public String getSection_body() {
        return section_body;
    }

    public void setSection_body(String section_body) {
        this.section_body = section_body;
    }
}
