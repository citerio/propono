package com.proponomultimedia.app.Model;

/**
 * Created by Jose Ricardo on 07/03/2017.
 */
public class Foto_Entity {

    private int image;
    private String section_head;
    private String section_body;
    private String section_body2;

    public Foto_Entity(int image, String section_head, String section_body, String section_body2) {
        this.image = image;
        this.section_head = section_head;
        this.section_body = section_body;
        this.section_body2 = section_body2;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSection_head() {
        return section_head;
    }

    public void setSection_head(String section_head) {
        this.section_head = section_head;
    }

    public String getSection_body() {
        return section_body;
    }

    public void setSection_body(String section_body) {
        this.section_body = section_body;
    }

    public String getSection_body2() {
        return section_body2;
    }

    public void setSection_body2(String section_body2) {
        this.section_body2 = section_body2;
    }
}
