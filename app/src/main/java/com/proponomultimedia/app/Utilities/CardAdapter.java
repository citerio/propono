package com.proponomultimedia.app.Utilities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proponomultimedia.R;
import com.proponomultimedia.app.Model.Entity;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jose Ricardo on 27/02/2017.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private Context nContext;
    private ArrayList<Entity> entities;


    public CardAdapter(Context context, ArrayList<Entity> entities){

        this.nContext = context;
        this.entities = new ArrayList<Entity>();
        this.entities.addAll(entities);
    }


    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(nContext).inflate(R.layout.card_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindNotification(entities.get(position));

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public void insert(Entity item) {
        entities.add(item);
        //notifyItemInserted(news.size()-1);

    }

    public void remove(int position) {
        entities.remove(position);
        notifyItemRemoved(position);

    }



    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swap(int firstPosition, int secondPosition){
        Collections.swap(entities, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public final TextView section_header;
        public final TextView section_body;

        public ViewHolder(View view){
            super(view);

            section_header = (TextView) view.findViewById(R.id.section_head);
            section_body = (TextView) view.findViewById(R.id.section_body);


        }

        public void bindNotification(Entity entity){

            this.section_header.setText(entity.getSection_head());
            this.section_body.setText(entity.getSection_body());

        }


    }


}
