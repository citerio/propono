package com.proponomultimedia.app.Utilities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proponomultimedia.R;
import com.proponomultimedia.app.Controller.Video_Player;
import com.proponomultimedia.app.Model.Foto_Entity;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jose Ricardo on 27/02/2017.
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private Context nContext;
    private ArrayList<Foto_Entity> entities;


    public VideoAdapter(Context context, ArrayList<Foto_Entity> entities){

        this.nContext = context;
        this.entities = new ArrayList<Foto_Entity>();
        this.entities.addAll(entities);
    }


    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(nContext).inflate(R.layout.video_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindNotification(entities.get(position));

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public void insert(Foto_Entity item) {
        entities.add(item);
        //notifyItemInserted(news.size()-1);

    }

    public void remove(int position) {
        entities.remove(position);
        notifyItemRemoved(position);

    }



    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swap(int firstPosition, int secondPosition){
        Collections.swap(entities, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public final TextView section_header;
        public final TextView section_body;
        public final ImageView section_image;

        public ViewHolder(View view){
            super(view);

            section_image = (ImageView)view.findViewById(R.id.section_image);
            section_header = (TextView) view.findViewById(R.id.section_head);
            section_body = (TextView) view.findViewById(R.id.section_body);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent video_player = new Intent(v.getContext(), Video_Player.class);
                    video_player.putExtra("id", (String) section_header.getTag());
                    video_player.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(video_player);
                }
            });

        }

        public void bindNotification(Foto_Entity entity){

            this.section_image.setImageResource(entity.getImage());
            this.section_header.setText(entity.getSection_head());
            this.section_body.setText(entity.getSection_body());
            this.section_header.setTag(entity.getSection_body2());

        }


    }


}
