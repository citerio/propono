package com.proponomultimedia.app.Utilities;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioTrack;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.spoledge.aacdecoder.AACPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

public class ShoutCast extends Service {

    private static final String TAG = "StreamService";
    boolean isPlaying;
    //SharedPreferences prefs;
    //SharedPreferences.Editor editor;
    private AACPlayer aac_player;
    private PlayerCallback aac_player_cb;
    private String radio_url = "http://shoutcast.pondi.hr:8000/";
    private String meta_title = "";
    private Intent intent_ma = new Intent("main_activity");


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Log.d(TAG, "onCreate");

        //prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //editor = prefs.edit();

        aac_player_cb = new PlayerCallback() {
            @Override
            public void playerStarted() {

            }

            @Override
            public void playerPCMFeedBuffer(boolean b, int i, int i1) {

            }

            @Override
            public void playerStopped(int i) {

                //editor.putString("meta_title", "");
                //editor.commit();
            }

            @Override
            public void playerException(Throwable throwable) {

            }

            @Override
            public void playerMetadata(String s, String s1) {

                if ("StreamTitle".equals( s ) || "icy-name".equals( s ) || "icy-description".equals( s )) {

                    meta_title = s1;

                } else return;

                //editor.putString("meta_title", meta_title);
                //editor.commit();


                intent_ma.putExtra("meta_title", meta_title);
                intent_ma.putExtra("code", 0);
                getApplicationContext().sendBroadcast(intent_ma);


            }

            @Override
            public void playerAudioTrackCreated(AudioTrack audioTrack) {

            }
        };

        aac_player = new AACPlayer(aac_player_cb);

        try {
            java.net.URL.setURLStreamHandlerFactory(new java.net.URLStreamHandlerFactory() {
                public java.net.URLStreamHandler createURLStreamHandler(String protocol) {
                    //Log.d(TAG, "Asking for stream handler for protocol: '" + protocol + "'");
                    if ("icy".equals(protocol))
                        return new com.spoledge.aacdecoder.IcyURLStreamHandler();
                    return null;
                }
            });
        } catch (Throwable t) {
            //Log.w(TAG, "Cannot set the ICY URLStreamHandler - maybe already set ? - " + t);
        }

        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //Log.d(TAG, "onStartCommand - StreamService");

        aac_player.playAsync(radio_url);

        //editor.putBoolean("isPlaying", true);
        //editor.commit();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //Log.d(TAG, "onDestroy - StreamService");

        aac_player.stop();

        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        //editor.putBoolean("isPlaying", false);
        //editor.commit();
    }

    /////////////////////////////////////////////////PHONE CALL///////////////////////////////////////////////////
    PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (state == TelephonyManager.CALL_STATE_RINGING) {

                intent_ma.putExtra("code", 1);
                getApplicationContext().sendBroadcast(intent_ma);
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };
}
