package com.proponomultimedia.app.Utilities;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proponomultimedia.R;
import com.proponomultimedia.app.Controller.News_Details;
import com.proponomultimedia.app.Controller.TabsActivity;
import com.proponomultimedia.app.Model.News;
import com.squareup.picasso.Picasso;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jose Ricardo on 25/07/2016.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context nContext;
    private ArrayList<News> news;


    public NewsAdapter(Context context, ArrayList<News> news){

        this.nContext = context;
        this.news = new ArrayList<News>();
        //this.news.addAll(news);
    }


    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(nContext).inflate(R.layout.news_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindNotification(news.get(position));

    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public void insert(News item) {
        news.add(item);
        //notifyItemInserted(news.size()-1);

    }

    public void remove(int position) {
        news.remove(position);
        notifyItemRemoved(position);

    }



    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swap(int firstPosition, int secondPosition){
        Collections.swap(news, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public final ImageView image;
        public final TextView header;
        public final TextView excerpt;
        public final TextView content;

        public ViewHolder(View view){
            super(view);

            image = (ImageView) view.findViewById(R.id.image);
            header = (TextView) view.findViewById(R.id.header);
            excerpt = (TextView) view.findViewById(R.id.excerpt);
            content = (TextView) view.findViewById(R.id.content);

            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    //Toast.makeText(v.getContext(), "Item clicked", Toast.LENGTH_LONG).show();
                    Fragment news_details = News_Details.newInstance(image.getTag().toString(), Html.toHtml((Spanned) header.getText()), content.getText().toString());
                    if(nContext instanceof TabsActivity){

                        //Toast.makeText(v.getContext(), "Inside fragment", Toast.LENGTH_LONG).show();
                        TabsActivity tabsActivity = (TabsActivity) nContext;
                        FragmentTransaction ft = tabsActivity.getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_news_list, news_details);
                        ft.addToBackStack(null);
                        ft.commit();
                        /*FragmentManager fm = mainActivity.getSupportFragmentManager();
                        fm.beginTransaction()
                                .remove(fm.findFragmentById(R.id.fragment_news_list))
                                .add(R.id.fragment_news_list, news_details)
                                .addToBackStack(null)
                                .commit();*/
                    }


                }
            });

        }

        public void bindNotification(News news){


            String image_url = Uri.encode(news.getImage(), "@#&=*+-_.,:!?()/~'%");

            /*image_url = image_url.replace("\\u010c", "Č");
            image_url = image_url.replace("\\u0106", "Ć");
            image_url = image_url.replace("\\u0107", "ć");
            image_url = image_url.replace("\\u010d", "č");
            image_url = image_url.replace("\\u0110", "Đ");
            image_url = image_url.replace("\\u0111", "đ");
            image_url = image_url.replace("\\u0160", "Š");
            image_url = image_url.replace("\\u0161", "š");
            image_url = image_url.replace("\\u017d", "Ž");
            image_url = image_url.replace("\\u017e", "ž");*/
            news.setImage(image_url);

            Picasso.with(nContext).load(news.getImage()).error(R.mipmap.propono_logo).fit().centerCrop().into(this.image);
            this.image.setTag(news.getImage());
            this.header.setText(Html.fromHtml(news.getHeader()));
            this.excerpt.setText(Html.fromHtml(news.getExcerpt()));
            this.content.setText(news.getContent());

        }


    }


}
