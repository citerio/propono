package com.proponomultimedia.app.Utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Jose Ricardo on 27/07/2016.
 */
public class NetworkConnection {

    //private String server_url = "http://www.radiodalmacija.hr/?json=get_recent_posts";

    public String FetchData(String server_url){

        URL url;
        HttpURLConnection connection =  null;
        String responseStr = "";

        try{

            url = new URL(server_url);
            connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestMethod("GET");
            //connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            //connection.setRequestProperty("Content-Length","" + Integer.toString(dataUrlParameters.getBytes().length));
            //connection.setRequestProperty("Content-Language", "en-US");
            //connection.setRequestProperty("User-Agent", "");
            //connection.setUseCaches(false);
            connection.setDoInput(true);
            //connection.setDoOutput(true);
            // Send request
            /*DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(dataUrlParameters);
            wr.flush();
            wr.close();*/
            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line = "";
            StringBuffer response = new StringBuffer();

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            responseStr = response.toString();


        }catch (Exception e){
            e.printStackTrace();
            responseStr = "No Internet Connection";
        }

        return responseStr;


    }



}
